# Health, Resources, Lifecycle
## Liveness probe

Health probes are one of the most important best practices. Usually a liveness- or readiness Probe consists of a call
to an api address, but different types are also possible, for example bash commands.

Read the file `pod-liveness.yaml` and create the pod it defines:
```
kubectl apply -f pod-liveness.yaml
```

When the container starts, it runs the following commands:

```
touch /tmp/healthy; 
sleep 30; 
rm -rf /tmp/healthy;
sleep 600;
```

If one only had to consider these commands, the container would stop running after 10:30 minutes.

Look at the pod events:
```
kubectl describe pod liveness-demo
```
The pod will get restarted after a certain time.
```
kubectl get pods
```
## Readiness probe
Now let's look at the file `pod-readiness.yaml` and start it up.
```
kubectl apply -f pod-readiness.yaml
```

Look at the pod's status:
```
kubectl get pods
```

Otherwise, the pod seems to be doing just fine:
```
kubectl logs -f readiness-demo
```

---
**Exercise**

Use `exec` on the pod `readiness-demo` and create the missing file to get the pod to achieve status `ready`.

<details><summary>spoiler - solution</summary>
<p>

```bash
kubectl exec readiness-demo -- touch /tmp/healthy
```

</p>
</details>

---

## Startup Probe
The pod defined in `pod-needing-startup-probe.yaml` only answers it's liveness probes after more than 30 seconds,
which results in a crash loop. Pods not starting up immediately is a very common behaviour when deployments are waiting for external dependencies to become available.
The answer to this problem is not to set the `initialDelay` to a very high value, but to either use the recently introduced
startup probe or an `initcontainer`.

```
kubectl apply -f pod-needing-startup-probe.yaml 
```

```
kubectl describe pods lifecycle-needs-startup
```

---
**Exercise**

Familiarize yourself with [startup probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-startup-probes)  
or [init containers](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)  and alter the manifest `pod-needing-startup-probe.yaml` so that it shows a `ready` status without increasing the initial delay.

---

## Resources

You can view a pods current resource usage with the following command
```
kubectl top pods 
```

First, examine the pod definition `pod-not-enough-resources.yaml` and apply it as it is: 
```
kubectl apply -f pod-not-enough-resources.yaml
```

Examine what happens when we set resource limits too low.
```
kubectl describe pod not-enough-resources-demo
```
---

**Note**: 
It is considered an important best practice to set a pods resources using the `requests` and `limits` fields. Otherwise, a failing pod can
pull down a whole node and with it a cluster.

---

Let's see what happens, when we try to create a pod that exceeds our namespaces resource quota:
```
kubectl apply -f deploy-hungry.yaml 
```

The status of the deployment we just created, does not mention our problem directy. Can you find some indicator of what is wrong?

<details><summary>hint</summary>
<p>
The problem is mentioned under "conditions"
</p>
</details>

```
kubectl describe deploy hungry
```

When looking at the replica sets, we can also find some hint about the underlying cause.

```
kubectl describe replicaset
```

Even if there is no resource quotas set for a namespace, you might also see something like this, when the cluster itself is out of resources:

```
Events:
  FirstSeen     LastSeen        Count   From                    SubObjectPath   Type            Reason                  Message
  ---------     --------        -----   ----                    -------------   --------        ------                  -------
  24m           0s              89      default-scheduler                       Warning         FailedScheduling        No nodes are available that match all of the following predicates:: Insufficient cpu (3).
```

## Lifecycle Handlers

In the file `pod-lifecycle-events`, you can see that the postStart command writes a message file to the Container's `/usr/share` directory. 
The preStop command shuts down nginx gracefully, although this is not necessary anymore in more recent versions of the container image. 

Create the pod:
```
kubectl apply -f pod-lifecycle-events.yaml
```

Verify that the pod is running and verify the contents of `/usr/share/message`:
```
kubectl exec -it lifecycle-demo -- cat /usr/share/message
```

