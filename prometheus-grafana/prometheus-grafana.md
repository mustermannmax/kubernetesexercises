# Prometheus Grafana and Spring Boot Actuator

[[_TOC_]]

## Deploying a spring boot application with custom metrics

We start with a little explanation. You won't have to do anything until exercise 1.

It is not necessary to clone the project [SpringMetricDemo](https://gitlab.com/mt-ag-k8s-training-course/springmetricdemo), the helm chart is available on your virtual machine.


This project showcases some basic micrometer metrics. First look at the [MetricsController](https://gitlab.com/mt-ag-k8s-training-course/springmetricdemo/-/blob/main/src/main/java/com/mtag/showcases/springboot/MetricsController.java) class:
```bash
    ...
    
    @Timed("demo_timer")
    @GetMapping("/random")
    public double getRandomMetricsData() throws InterruptedException {
        demoCounter.increment();
        TimeUnit.MILLISECONDS.sleep((long) demoCounter.count()*100);
        return getRandomNumberInRange(0, 100);
    }
    
    ...
```

Each time the `/random` endpoint is called, the call will take 100ms longer then before. Although this is a primitive example, it is not unheard of that some unacceptable growth in execution times, not being caught in automated tests.
Luckily we added some application specific monitoring to our project.

---
In `application.properties`:
```bash
...
management.endpoints.web.exposure.include=health,info,prometheus
...
```

---
In `pom.xml`:
```bash
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
        <dependency>
            <groupId>io.micrometer</groupId>
            <artifactId>micrometer-registry-prometheus</artifactId>
            <scope>runtime</scope>
        </dependency>
        ...
    </dependencies>
```

And finally in our helm chart:
```bash
apiVersion: monitoring.coreos.com/v1 # servicemonitor.yaml
kind: ServiceMonitor
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    release: kube-prometheus-stack
  {{- include "chart.labels" . | nindent 4 }}
spec:
  endpoints:
    - interval: 15s
      path: /actuator/prometheus
      targetPort: 8080
  namespaceSelector:
    matchNames:
      - {{ .Release.Namespace }}
  selector:
    matchLabels:
      {{- include "chart.labels" . | nindent 6 }}
```

### Exercise 1
- Change `!changeme!` in `values-sample.yaml` to use another hostname (not in gitlab, but on your virtual machine!)
- Deploy the helm chart already available on your virtual machine to your namespace
  ```bash
    helm upgrade --install spring-metrics-demo ./chart -f values-sample.yaml
  ```
- Wait a moment and check if all resources are ready `kubectl get pods,ing,svc,cert`
- Call the url from your browser (e.g.  https://!changeme!.mt-k8s.eu)

## Exploring Prometheus Metrics

- Navigate to the `/random` endpoint, either using the link in the header menu or with https://!changeme!.mt-k8s.eu/random
- Refresh the site multiple times and watch as the requests are taking longer each time
- Now call the prometheus metrics endpoint at `/actuator/prometheus` - Can you find the custom metrics starting `demo_counter` and `demo_timer`?

```bash
...
demo_counter_total{application="SpringDemo",} 13.0
...
demo_timer_seconds_count{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 13.0
demo_timer_seconds_sum{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 9.187199498
...
demo_timer_seconds_max{application="SpringDemo",exception="None",method="GET",outcome="SUCCESS",status="200",uri="/random",} 1.3057129
```

Now head over to [prometheus](https://prometheus.mt-k8s.eu) and take your time exploring the ui. Check if your application is known to prometheus by navigating to `targets`:
![alt text](./targets.png "Targets")


Head over to graph next and try some simple queries:
![alt text](./graph.png "Graph")

All deployments of this endpoint will provide the same metric names. You can filter a metric for example using your namespace by appending `{namespace="participant-9"}`.
Start by querying the total execution time of the function with:
```
demo_timer_seconds_count{namespace="participant-9"}
```
### Exercise 2

Try to write the following queries.

**Note:** Look at the [cheat sheet](https://promlabs.com/promql-cheat-sheet/) for more examples
- Was the average value of `demo_timer_seconds_max` bigger than `1.0`?

avg(demo_timer_seconds_max{namespace="participant-9"})>1.0

- The sum of `demo_counter_total` over all instances and namespaces:

<details><summary>spoiler - solution</summary>
<p>

```bash
sum(demo_counter_total)
```

</p>
</details>

- (Bonus)  The increase per minute of `demo_counter_total` using the `increase` function

<details><summary>spoiler - solution</summary>
<p>

```bash
increase(demo_counter_total{namespace="participant-9"}[1m])
```

</p>
</details>

- (Bonus) The rate of change of the counter in the last 1m using the `rate` function

<details><summary>spoiler - solution</summary>
<p>

```bash
rate(demo_counter_total{namespace="participant-9"}[1m])
```

</p>
</details>

- (Bonus) Prediction of the value of `demo_timer_seconds_max[1h]` in one hour

<details><summary>spoiler - solution</summary>
<p>

```bash
predict_linear(demo_timer_seconds_max{namespace="participant-9"}[1h],3600)
```

</p>
</details>

# Grafana

### Exercise 3

Our goal is to make an automatic alert apear in MS Teams when `demo_timer_seconds_max{namespace=~"participant-x"}` reaches a certain point.

Head over to [grafana](https://grafana.mt-k8s.eu/) and login using `user:admin password:prom-operator` as credentials

1. Create a new folder

![alt text](./grafana01.png "Create Folder")

2. Create a new dashboard

3. Add a new panel

![alt text](./grafana03.png "Add a new panel")

4. Fill `A` with the metric `demo_timer_seconds_max{namespace=~"participant-x"}` and click `Run queries` to check if everything is working correctly (a graph will apear in the area above).

![alt text](./grafana04.png "Fill A")

5. Click on `+ Expression`

![alt text](./grafana05.png "Add Expression")

6. Change `operation` to `Classic condition`

![alt text](./grafana06.png "Classic condition")

7. Define the condition with WHEN `max()` OF `A` IS ABOVE `X` with X a value `demo_timer_seconds_max{namespace=~"participant-x"}` has not yet reached, but will be reached after a few additional calls of https://!changeme!.mt-k8s.eu/random

![alt text](./grafana07.png "Classic condition")

8. Click on `Apply` in the right top corner

9. Save your dashboard.

![alt text](./grafana09.png "dashboard")

10. Open your panel from before in `Edit` mode

![alt text](./grafana10.png "Edit panel")

11. Switch to the `Alert` tab

![alt text](./grafana11.png "Switch to Alert tab")

12. Create an Alert rule

13. Adjust the Alert evaluation behavior to `Evaluate every 1m for 1s`

![alt text](./grafana13.png "Adjust the alert evaluation behavior")

14. Add a label `channel = msteams`

![alt text](./grafana14.png "label channel = msteams")

15. Trigger https://!changeme!.mt-k8s.eu/random until `demo_timer_seconds_max{namespace=~"participant-x"}` is higher than the choosen value X from 9.

16. Check the teams channel. This can take a few minutes. If no Grafana-Subchannel exists or is visible, alert the tutor.

---

### Exercise 4

Take your time exploring and try to create a dashboard for our sample application. Nicest dashboard wins. 😉
