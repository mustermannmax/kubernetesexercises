# Building Container Images

## Create a Dockerfile

Navigate to the folder `kubernetesexercises/container-images/create-dockerfile`.

```
cd create-dockerfile
```
Create your `Dockerfile` for building your image 
```
touch Dockerfile
```

For the start we fill our `Dockerfile` with the contents below
```
FROM docker.io/nginx:alpine
COPY . /usr/share/nginx/html
```

Next build the image

```
podman build -t creating-dockerfile:v1 .
```

## Running the dockerfile you created
Launch the image in background:
```
podman run -d -p 8080:80 --name creating-dockerfile creating-dockerfile:v1
```
Once it has started, try using `curl` to communicate with it
```
curl 127.0.0.1:8080
```

## Exec into the dockerfile

Using this command you can open a shell inside a running container
```shell
podman exec -it creating-dockerfile /bin/sh
```

Try to view the contents of `entrypoint.sh`
```shell
cat docker-entrypoint.sh
```

You can exit the shell by typing `exit` or using `Control + D`

Now stop the running container
```shell
podman stop creating-dockerfile
```

## Copying files from // to containers

We are now switching to another directory: `kubernetesexercises/container-images/copying-files`.

The file `output.txt` is missing in the Containerimage `copying-files/CopyingFiles.Dockerfile` Modify the `Dockerfile` by using the `COPY` Command for copying `output.txt` to `CopyingFiles.Dockerfile`.

<details><summary>spoiler - solution</summary>
<p>

```bash
FROM docker.io/alpine

WORKDIR /app/
COPY output.txt /app/output.txt

ENTRYPOINT cat output.txt
```

</p>
</details>

Now build and run the image.

```
podman build -t copying-files:v1 --file CopyingFiles.Dockerfile .
podman run --rm  copying-files:v1
```

## Pushing to registry

Try pushing the image you created to the container registry included in our gitlab project.

First login to the registry:
```
podman login registry.gitlab.com -u gitlab+deploy-token-748654 -p QeMuyGpcpsBsnCuxVoyz
```

Next tag the image you created. Please choose a random tag:
```
export RANDOM_TAG=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13 ; echo '')
podman tag copying-files:v1 registry.gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/docker-image:$RANDOM_TAG
podman push registry.gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/docker-image:$RANDOM_TAG
```

You can find your container image by navigating to [the registry in gitlab.com](https://gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/container_registry/2654704)


## Trivy

Use trivy to get a list of vulnerabilities for the image you just pushed

```shell
trivy image registry.gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/docker-image:$RANDOM_TAG
```

Try to scan some of the other base images by first doing `podman pull <IMAGE>` and then running `trivy image <IMAGE>`

* gcr.io/distroless/python3-debian11:debug
* docker.io/nginx
* registry.access.redhat.com/ubi8/ubi
* distroless/static-debian10
* scratch
* debian:bullseye-slim

Can you find those without CVEs?

## Environment Variables

We are now switching to another directory: `kubernetesexercises/container-images/env-var`.

Edit dockerfile `EnvVar.Dockerfile` by adding the environment variable `ENV_VAR`.

Now try building and executing the container:
```
podman build -t envar --file EnvVar.Dockerfile .
podman run --rm envar
```

You can also set environment Variables after building a container:
```yaml
podman run --rm -e ENV_VAR=Hello envar
```

## Reducing the number of layers

We are now switching to another directory: `kubernetesexercises/container-images/layers`.

Here, you can find a Dockerfile with unnecesarily many layers. Try to edit the dockerfile so that there are less layers.

You can validate the number of layers created by building the image and then running

```bash
skopeo inspect containers-storage:localhost/<your-image-name>:<your-image-tag>
```


### Useful podman commands

|  podman command | description   |
|---|---|
|  `podman info` | Check host information |
|  `podman pull [image](:[tag])` | Copies images from registry onto local machine. Default is the latest tag, otherwise tag must be specified |
|  `podman images (-a)` | List images on local machine. Without `-a` intermediate image layers are filtered out |
|  `podman run (-t/--tty) (-i/--interactive) [image](:[tag])` | Run a process in a new container. Can be made accessible in the terminal with the flags `-it` |
|  `podman run (-t/--tty) (-i/--interactive) [image](:[tag]) [command]` | Run a process in a *new* container with a command (e.g.: `podman run -t test_image echo "hello"`) |
|  `podman exec (-t/--tty) (-i/--interactive) [container] [command]` | Run a command in a *running* container   (e.g.: `podman exec -it testcontainer /bin/sh`) |
|  `podman run (-d/--detach) (-p/--publish [ip]:[port]) (--name [name]) [image](:[tag])` | Run a detached new container with an open port and a specific name |
|  `podman run -rm [image](:[tag])` | Run a container and remove it automatically when it exits |
|  `podman build (-t/--tag [image name](:[tag])) [docker-/containerfile path]` |  Builds an image from a Dockerfile or Containerfile |
|  `podman stop [container]` |   Stops a running container |
|  `podman start [container]` |   Starts an existing container |
|  `podman rm (-f/--force) [container]` |  Removes a container from the host. `-f` is necessary if the container is running or unusable |
|  `podman rmi (-f/--force) [image(:[tag])]` |  Removes an image from the host. The `-f` will remove all containers that use that image |
|  `podman rmi (-f/--force) --all` |  Removes *all* images from the host. The `-f` will remove all containers that use these images |
|  `podman tag [image(:[tag])] [new name(:[tag])]` | Adds a new name (and tag if specified) to an image  |
|  `podman login [adress] -u [username] -p [password]` | Logs into a registry |
|  `podman logout [adress]` | Logs out of a registry |
|  `podman push [image(:[tag])]` | Deploys an image (latest if not a specified tag) to the registry |
|  `trivy image [image(:[tag])]` | Scans an image for vulnerabilities |
|  `skopeo inspect [image(:[tag])] (\| jq)` | Inspect layers of an image |

