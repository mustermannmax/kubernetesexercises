# Environment Variables, ConfigMaps and Secrets

## Environment variables and commands
Look into the file `pod-print-something.yaml`. It contains two useful functionalities we didn't see before. Can you name them?
```bash
kubectl apply -f pod-print-something.yaml
```

This pod just printed something to stdout and then exited. To view it's output, use `kubectl logs`:

```bash
kubectl logs print-something
```
## Configmaps

Let's create a configmap using the commandline:
```bash
kubectl create configmap nginx-html --from-file=./index.html
```

The pod `pod-configmap-mount.yaml` mounts your configmap in the `/usr/share/nginx/html` folder.
```bash
kubectl apply -f pod-configmap-mount.yaml
```

Let's view its contents:
```bash
kubectl exec nginx-html -- cat /usr/share/nginx/html/index.html
```
---
**Exercise**


* Find a way to call the pod we just created on port 80 with your browser.
* Change the contents of the ConfigMap `nginx-html` using e.g. `kubectl edit` and call the pod again. What happens?

<details><summary>spoiler - solution</summary>
<p>

```bash
kubectl expose pod nginx-html --port=80 --name=my-nginx-service --type=LoadBalancer
```

</p>
</details>

---

## Secrets

Let's create a secret using the command line
```bash
kubectl create secret generic some-secret --from-literal=username=admin --from-literal=password=1234test
```

Now look into the secret:
```bash
kubectl get secret/some-secret -o yaml
```

The values `username` and `password` are only shown `base64` encoded. Of course, this doesn't
provide any real security.

To view the values, you can pipe them into `base64 --decode`:

```bash
echo YWRtaW4= | base64 -d
```

Next look at the file `pod-with-env-from-secret` and understand how the secret you created is mounted here to provide
environment variables to this pod.

Now apply the manifest file:

```bash
kubectl apply -f pod-with-env-from-secret.yaml
```

List the pod's environment variables.
```bash
kubectl exec secret-demo-pod -- printenv
```
We can see the pod's environment.
```
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=secret-demo-pod
SECRET_PASSWORD=Ash1eesaireM
SECRET_USERNAME=admin
...
```

