# Pods, configmaps and secrets

### Create a configmap named `kata-cm` with values `foo=bar`,`hello=hello`

<details><summary>spoiler</summary>
<p>

```bash
kubectl create configmap kata-cm --from-literal=foo=bar --from-literal=hello=hello
```

</p>
</details>

### Create and display a configmap from a file

Create a file with

```bash
echo -e "something=something\nsomething-else=somethingsomething-else > file.txt
```

<details><summary>spoiler</summary>
<p>

```bash
kubectl create cm configmap-from-file --from-file=new-key=file.txt
kubectl describe cm configmap-from-file
kubectl get cm configmap-from-file -o yaml
```
</p>
</details>

### Create a secret called some-random-secret that gets key/value from a file

Create a file called username with the value dbuser:

```
echo dbuser > username
```

<details><summary>spoiler</summary>
<p>

```bash
kubectl create secret generic some-random-secret --from-file=username
```

</p>
</details>

### Get the content of the secret you just created

<details><summary>spoiler</summary>
<p>

```
kubectl get secret some-random-secret -o yaml
echo -n YnVzZXIK | base64 -d
```

</p>
</details>


### Create a nginx pod that mounts the secret some-random-secret to `/opt/username`

<details><summary>spoiler</summary>
<p>

```
kubectl run busybox --image=busybox --restart=Never -o yaml --dry-run=client > busybox.yaml
vi busybox.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: nginx
  name: nginx
spec:
  volumes:
  - name: username
    secret:
      secretName: some-random-secret
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: nginx
    volumeMounts:
    - name: foo
      mountPath: /opt/username
```
</p>
</details>

### Create a busybox pod that runs 'ls -la /` and then show the pod's logs:

<details><summary>spoiler</summary>
<p>

```bash
kubectl run busybox-logs --image=busybox --restart=Never -- /bin/sh -c 'ls -la /'
kubectl logs busybox-logs
```
</p>
</details>

### Get the current resource usage of your pods and nodes and the available resourcequotas inside your namespace

<details><summary>spoiler</summary>
<p>

```
kubectl top nodes
kubectl top pods
kubectl get resourcequota
```

</p>
</details>

### Create a configMap 'env-vars' with values 'ENV_VAR=random', 'OTHER_ENV_VAR=other'. Load this configMap as env variables for a pod with e.g. the busybox image

<details><summary>spoiler</summary>
<p>

```bash
kubectl create configmap env-vars --from-literal=ENV_VAR=random --from-literal=OTHER_ENV_VAR=other7
kubectl run --restart=Never busybox --image=busybox -o yaml --dry-run=client > pod.yaml
vi pod.yaml
```

```YAML
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: busybox
  name: busybox
spec:
  containers:
  - image: busybox
    imagePullPolicy: IfNotPresent
    name: busybox
    resources: {}
    envFrom:
    - configMapRef:
        name: env-vars
```

</p>
</details>

### Create a pod with the busybox image and requests.cpu:10m, requests.memory=256Mi and limits.cpu=200m, limits.memory=512Mi

<details><summary>spoiler</summary>
<p>

```bash
kubectl run busybox2 --image=busybox --dry-run=client -o yaml > pod.yaml
vi pod.yaml
```

```YAML
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: busybox
  name: busybox
spec:
  containers:
  - image: busybox
    imagePullPolicy: IfNotPresent
    name: busybox
    resources:
      requests:
        memory: "256Mi"
        cpu: 10m
      limits:
        memory: "512Mi"
        cpu: 200m
```
</p>
</details>

# Deployments and Services
### Create a deployment named hello-app for 'gcr.io/google-samples/hello-app@sha256:2b0febe1b9bd01739999853380b1a939e8102fd0dc5e2ff1fc6892c4557d52b9' and 3 replicas. Label it as 'hello=app'. Declare that the containers accepts traffic on port 8080 

<details><summary>spoiler</summary>
<p>

```bash
kubectl create deploy hello-app --image=gcr.io/google-samples/hello-app@sha256:2b0febe1b9bd01739999853380b1a939e8102fd0dc5e2ff1fc6892c4557d52b9 --port=8080 --replicas=3
kubectl label deployment hello-app --overwrite hello=app
```
</p>
</details>

### Call one of the pods you created locally using curl

<details><summary>spoiler</summary>
<p>

```bash
kubectl port-forwad deploy/hello-app 8080:8080 &
curl 127.0.0.1:8080
killall kubectl
```
</p>
</details>

### Create a service of type loadbalancer that exposes the deployment on port 80. Check the services and it's endpoints.

<details><summary>spoiler</summary>
<p>

```bash
kubectl expose deployment hello-app --port=80 --type=LoadBalancer
kubectl describe svc hello-app
```
</p>
</details>

