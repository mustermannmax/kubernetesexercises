# Gitlab CI exercises

[[_TOC_]]

**Note:** Using the official Gitlab CI Reference Guide will help you with this exercise:
https://docs.gitlab.com/ee/ci/yaml/

## Using Gitlab CI Variables


---


```yaml
variables:
  CI_VARIABLE: "ROOT"

stages:
  - test

env-var-job:
  stage: test
  variables:
    CI_VARIABLE: "IN_JOB"
  script:
    - env
    - echo "$CI_VARIABLE"
```

Create a `.gitlab-ci.yml` with the above content.

---

![img_4.png](img_4.png)

Now try to set a gitlab ci variable via the gitlab gui with different value and observe the result.

---
Try tagging a commit, creating a branch, or a merge request and observe the changes to Gitlab CI environment variables that are shown
inside the job's logs.

## Creating a pipeline trigger



![img_5.png](img_5.png)
Create a trigger token in `Menu -> Projects -> Settings -> CI/CD -> Pipeline Triggers`.



![img_6.png](img_6.png)
Copy the token and trigger the pipeline using the `curl` command shown after creating the token.
```bash
curl -X POST --fail -F token=<YOUR_TOKEN> -F ref=REF_NAME https://gitlab.com/api/v4/projects/<YOUR_PROJECT_ID>/trigger/pipeline
```

Now try passing the ci variable again using variables [VARIABLE]=VALUE

```bash
curl -X POST --fail -F token=<YOUR_TOKEN> -F ref=REF_NAME -F "variables[<VARIABLE>]=<VALUE>" https://gitlab.com/api/v4/projects/<YOUR_PROJECT_ID>/trigger/pipeline
```

## Using schedules

Finally lets schedule the pipeline to run regularly using the _Schedules_ function

![img.png](img.png)

### Exercise 1

![img_2.png](img_2.png)

Configure you pipeline to run scheduled in at `13:05` Today

## Creating a maven gitlab ci pipeline from scratch

Add the following snippet to your `.gitlab-ci.yml`

```yaml
variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  MAVEN_CLI_OPTS: "-Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true --batch-mode -Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS_SKIPTESTS: '-Dcheckstyle.skip=true -DskipTests=true'


#--------------------------------------------------------------------------------
# Build
#--------------------------------------------------------------------------------

maven-build:
  image: maven:latest
  stage: maven-build
  script:
    - mvn package $MAVEN_CLI_OPTS  $MAVEN_CLI_OPTS_SKIPTESTS
    - PROJECT_VERSION=$(mvn $MAVEN_CLI_OPTS  help:evaluate -Dexpression=project.version -q -DforceStdout)
    - echo Project Version is $PROJECT_VERSION
    - echo "PROJECT_VERSION=$PROJECT_VERSION" >> build.env
  artifacts:
    reports:
      dotenv: build.env
    paths:
      - '**/target/*.jar'
    expire_in: 1 week

#--------------------------------------------------------------------------------
# Test
#--------------------------------------------------------------------------------

maven-test:
  image: maven:latest
  stage: maven-test
  script:
    -  mvn test $MAVEN_CLI_OPTS
  artifacts:
    reports:
      junit:
        -  target/site/jacoco/jacoco.xml
```
### Exercise 2
- Something will probably be missing, can you fix it?
  
  _Spoiler_:
  ```bash
  $ echo c3RhZ2VzOgogIC0gbWF2ZW4tYnVpbGQKICAtIG1hdmVuLXRlc3QKICAtIGRlcGxveQ== | base64 -d
  ```
- _Bonus_: Modify the pipeline to not run for merge requests using [workflow](https://docs.gitlab.com/ee/ci/yaml/#workflow) rules
  
  _Spoiler_:
  ```bash
  $ echo d29ya2Zsb3c6CiAgcnVsZXM6CiAgICAtIGlmOiAnJENJX1BJUEVMSU5FX1NPVVJDRSA9PSAibWVyZ2VfcmVxdWVzdF9ldmVudCInCiAgICAgIHdoZW46IG5ldmVyCiAgICAtIHdoZW46IGFsd2F5cw== | base64 -d
  ```
- _Bonus_: Add [caching for dependencies](https://docs.gitlab.com/ee/ci/caching/index.html#cache-python-dependencies) for the path `.m2/repository`
  
  _Spoiler_:
  ```bash
  $ echo Y2FjaGU6CiAgcGF0aHM6CiAgICAtIC5tMi9yZXBvc2l0b3J5Cg== | base64 -d
  ```


### Bonus Exercise 3

- Add a Gitlab CI Job for building a docker image using [Gitlab's Template](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) as a reference.
  
  _Spoiler_:
   ```bash
   $ echo  Iy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCiMgQnVpbGQKIy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCgpidWlsZDoKICBzdGFnZTogZGVwbG95CiAgaW1hZ2U6CiAgICBuYW1lOiBnY3IuaW8va2FuaWtvLXByb2plY3QvZXhlY3V0b3I6ZGVidWcKICAgIGVudHJ5cG9pbnQ6IFsiIl0KICBzY3JpcHQ6CiAgICAtIG1rZGlyIC1wIC9rYW5pa28vLmRvY2tlcgogICAgLSB8LQogICAgICAgS0FOSUtPUFJPWFlCVUlMREFSR1M9IiIKICAgICAgIEtBTklLT0NGRz0iXCJhdXRoc1wiOntcIiR7Q0lfUkVHSVNUUll9XCI6e1wiYXV0aFwiOlwiJChwcmludGYgIiVzOiVzIiAiJHtDSV9SRUdJU1RSWV9VU0VSfSIgIiR7Q0lfUkVHSVNUUllfUEFTU1dPUkR9IiB8IGJhc2U2NCB8IHRyIC1kICdcbicpXCJ9fSIKICAgICAgIGlmIFsgIngke2h0dHBfcHJveHl9IiAhPSAieCIgLW8gIngke2h0dHBzX3Byb3h5fSIgIT0gIngiIF07IHRoZW4KICAgICAgICAgS0FOSUtPQ0ZHPSIke0tBTklLT0NGR30sIFwicHJveGllc1wiOiB7IFwiZGVmYXVsdFwiOiB7IFwiaHR0cFByb3h5XCI6IFwiJHtodHRwX3Byb3h5fVwiLCBcImh0dHBzUHJveHlcIjogXCIke2h0dHBzX3Byb3h5fVwiLCBcIm5vUHJveHlcIjogXCIke25vX3Byb3h5fVwifX0iCiAgICAgICAgIEtBTklLT1BST1hZQlVJTERBUkdTPSItLWJ1aWxkLWFyZyBodHRwX3Byb3h5PSR7aHR0cF9wcm94eX0gLS1idWlsZC1hcmcgaHR0cHNfcHJveHk9JHtodHRwc19wcm94eX0gLS1idWlsZC1hcmcgbm9fcHJveHk9JHtub19wcm94eX0iCiAgICAgICBmaQogICAgICAgS0FOSUtPQ0ZHPSJ7ICR7S0FOSUtPQ0ZHfSB9IgogICAgICAgZWNobyAiJHtLQU5JS09DRkd9IiA+IC9rYW5pa28vLmRvY2tlci9jb25maWcuanNvbgogICAgLSA+LQogICAgICAva2FuaWtvL2V4ZWN1dG9yCiAgICAgIC0tY29udGV4dCAiJHtDSV9QUk9KRUNUX0RJUn0iCiAgICAgIC0tZG9ja2VyZmlsZSAiJHtDSV9QUk9KRUNUX0RJUn0vRG9ja2VyZmlsZSIKICAgICAgIiR7S0FOSUtPUFJPWFlCVUlMREFSR1N9IgogICAgICAtLWRlc3RpbmF0aW9uICIke0NJX1JFR0lTVFJZX0lNQUdFfToke0NJX0NPTU1JVF9TSE9SVF9TSEF9Igo=| base64 -d
   ```
- Use a pipeline variable called `IMAGE_TAG` inside the destination instead of `CI_COMMIT_TAG`
- Modify the `kaniko` job to only run for `main` branch and manually triggered using [rules](https://docs.gitlab.com/ee/ci/yaml/#rules):
  
  _Spoiler_:
  ```bash
  $ echo cnVsZXM6CiAgLSBpZjogJyRDSV9DT01NSVRfQlJBTkNIID1+IC9eKG1hc3RlcnxtYWluKSQvJwogICAgd2hlbjogbWFudWFs | base64 -d
  ```
- Modify the rules further, to use the `CI_COMMIT_TAG` as container image tag in case of a git commit and
  `CI_COMMIT_SHORT_SHA` in case of a commit in the `main` branch
    _Spoiler_:
  ```bash
  $ echo YnVpbGQ6CiAgc3RhZ2U6IGRlcGxveQogIGltYWdlOgogICAgbmFtZTogZ2NyLmlvL2thbmlrby1wcm9qZWN0L2V4ZWN1dG9yOmRlYnVnCiAgICBlbnRyeXBvaW50OiBbIiJdCiAgc2NyaXB0OgogICAgLSBta2RpciAtcCAva2FuaWtvLy5kb2NrZXIKICAgIC0gfC0KICAgICAgIEtBTklLT1BST1hZQlVJTERBUkdTPSIiCiAgICAgICBLQU5JS09DRkc9IlwiYXV0aHNcIjp7XCIke0NJX1JFR0lTVFJZfVwiOntcImF1dGhcIjpcIiQocHJpbnRmICIlczolcyIgIiR7Q0lfUkVHSVNUUllfVVNFUn0iICIke0NJX1JFR0lTVFJZX1BBU1NXT1JEfSIgfCBiYXNlNjQgfCB0ciAtZCAnXG4nKVwifX0iCiAgICAgICBpZiBbICJ4JHtodHRwX3Byb3h5fSIgIT0gIngiIC1vICJ4JHtodHRwc19wcm94eX0iICE9ICJ4IiBdOyB0aGVuCiAgICAgICAgIEtBTklLT0NGRz0iJHtLQU5JS09DRkd9LCBcInByb3hpZXNcIjogeyBcImRlZmF1bHRcIjogeyBcImh0dHBQcm94eVwiOiBcIiR7aHR0cF9wcm94eX1cIiwgXCJodHRwc1Byb3h5XCI6IFwiJHtodHRwc19wcm94eX1cIiwgXCJub1Byb3h5XCI6IFwiJHtub19wcm94eX1cIn19IgogICAgICAgICBLQU5JS09QUk9YWUJVSUxEQVJHUz0iLS1idWlsZC1hcmcgaHR0cF9wcm94eT0ke2h0dHBfcHJveHl9IC0tYnVpbGQtYXJnIGh0dHBzX3Byb3h5PSR7aHR0cHNfcHJveHl9IC0tYnVpbGQtYXJnIG5vX3Byb3h5PSR7bm9fcHJveHl9IgogICAgICAgZmkKICAgICAgIEtBTklLT0NGRz0ieyAke0tBTklLT0NGR30gfSIKICAgICAgIGVjaG8gIiR7S0FOSUtPQ0ZHfSIgPiAva2FuaWtvLy5kb2NrZXIvY29uZmlnLmpzb24KLSA+LQogICAgICAva2FuaWtvL2V4ZWN1dG9yCiAgICAgIC0tY29udGV4dCAiJHtDSV9QUk9KRUNUX0RJUn0iCiAgICAgIC0tZG9ja2VyZmlsZSAiJHtDSV9QUk9KRUNUX0RJUn0vRG9ja2VyZmlsZSIKICAgICAgIiR7S0FOSUtPUFJPWFlCVUlMREFSR1N9IgogICAgICAtLWRlc3RpbmF0aW9uICIke0NJX1JFR0lTVFJZX0lNQUdFfToke0lNQUdFX1RBR30iCnJ1bGVzOgogIC0gaWY6ICckQ0lfQ09NTUlUX0JSQU5DSCA9fiAvXihtYXN0ZXJ8bWFpbikkLycKICAgIHdoZW46IG1hbnVhbAogICAgdmFyaWFibGVzOgogICAgICBJTUFHRV9UQUc6ICRDSV9DT01NSVRfU0hPUlRfU0EKICAtIGlmOiAnJENJX0NPTU1JVF9UQUcnCiAgICB2YXJpYWJsZXM6IAogICAgICBJTUFHRV9UQUc6ICRDSV9DT01NSVRfVEFHCg== | base64 -d
  ```

