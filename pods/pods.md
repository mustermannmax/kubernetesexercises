# Pods

Navigate to the folder `kubernetesexercises/pods`.

## Creating a pod with kubectl

First lets start by creating a pod declaratively:

```bash
kubectl run nginx --image nginx
```

Look at the pod as it is started:
```bash
kubectl get pods
```

Look at the yaml manifests of the pods:
```bash
kubectl get pods -o yaml
```


You will also find some events relating to your pod:
```bash
kubectl get events
```

You can view the IPs again by adding -o wide:
```bash
kubectl get pods -o wide
```

**Please note the pod's IP**

Now let's delete the pod then start it again
```bash
kubectl delete pod nginx && kubectl run nginx --image nginx
```

Look at the pod again:
```bash
kubectl get pods -o wide
```

**Do you see what changed?**



## Creating a pod from yaml and proxying into it

Next we will create a pod from a `yaml` file

```bash
kubectl apply -f pod.yaml
```

Forward the pods traffic using the `port-forward` command
```bash
kubectl port-forward http-echo 8080:8080
```

Open another shell and try communicating with it

```bash
curl 127.0.0.1:8080
```

## Init Containers

Init containers can be really useful, for example when waiting for a database to be ready. The command for the init container in `initcontainer-with-error.yaml` does not finish. Let's deploy the pod anyway to see how it behaves:

```bash
kubectl apply -f initcontainer-with-error.yaml
```

Let's try one of the most helpful debug commands:
```bash
kubectl describe pods initcontainer-pod
```

Fix the `initcontainer-with-error.yaml` then delete the pod and apply it again

```bash
kubectl delete -f initcontainer-with-error.yaml
kubectl apply -f initcontainer-with-error.yaml
```
