# Preparation
### Switch to the other cluster having network policies enabled using the KUBECONFIG  env varibale
```
export KUBECONFIG=~/.kube/config-netpol
```
### Create two deployments named `backend using` and `web` using the image `k8s.gcr.io/echoserver:1.4` and 2 replicas. Declare that the containers accepts traffic on port 8080

<details><summary>spoiler</summary>
<p>

```bash
kubectl create deploy backend --image=k8s.gcr.io/echoserver:1.4 --port=8080 --replicas=2
kubectl create deploy web --image=k8s.gcr.io/echoserver:1.4 --port=8080 --replicas=2

```
</p>
</details>

### Create a ClusterIP service exposing the port 80 for both deployments

<details><summary>spoiler</summary>
<p>

```bash
kubectl expose deploy/web --port 80 --target-port 8080
kubectl expose deploy/backend --port 80 --target-port 8080
```

</p>
</details>

### Validate the pods have access to all services using curl

<details><summary>spoiler</summary>
<p>

```bash
kubectl exec -it deploy/backend -- curl web
kubectl exec -it deploy/web -- curl backend #.participant-x.svc.cluster.local
...
```

</p>
</details>

### Apply the network policy from `network-policy-deny-all-ingress.yaml` and verify the no connections between pods are possible anymore


<details><summary>spoiler</summary>
<p>

```bash
kubectl apply -f network-policy-deny-all.yaml
kubectl exec -it deploy/web -- curl backend -m 1
kubectl exec -it deploy/backend -- curl web -m 1
...
```

</p>
</details>

### Now apply the network policy from `network-policy-backend-allow-ingress-web.yaml` and verify we realized access from web pods to backend pods but not the other way around.


<details><summary>spoiler</summary>
<p>

```bash
kubectl apply -f network-policy-backend-allow-ingress-web.yaml
kubectl exec -it deploy/web -- curl backend -m 1
kubectl exec -it deploy/backend -- curl web -m 1
...
```

</p>
</details>

### Now try the same from another participant's namespace


<details><summary>spoiler</summary>
<p>

```bash
kubectl get deploy -A # look for existing deployments
kubectl exec -it deploy/web -n participant-z -- curl backend.participant-x.svc.cluster.local -m 1 
kubectl exec -it deploy/backend-n participant-z  -- curl web.participant-x.svc.cluster.local -m 1
...
```

</p>
</details>

### Deny all egress by applying `network-policy-deny-all-egress.yaml`. Check if connection between deploy/backend and svc/web is still permitted


<details><summary>spoiler</summary>
<p>

```bash
kubectl apply -f network-policy-deny-all-egress.yaml
kubectl exec -it deploy/backend -- curl web -m 1
...
```

</p>
</details>

### Finally, we selectivly allow egress traffic for DNS and from web to backend pods. Verify if it is working
<details><summary>spoiler</summary>
<p>

```bash
kubectl apply -f  network-policy-web-allow-egress-backend.yaml
kubectl exec -it deploy/backend -- curl web
...
```

</p>
</details>

### Delete all egress networkpolicies, then modify `backend-allow-ingress-web` to include a namespace selector with the label `kubernetes.io/metadata.name`
<details><summary>spoiler</summary>
<p>

```bash
kubectl delete netpol default-deny-egress web-allow-egress-backend
```

```yaml
  ingress:
    - from:
        - podSelector:
            matchLabels:
              app: web
          namespaceSelector:
            matchLabels:
              kubernetes.io/metadata.name: participant-x
```


```bash
kubectl exec -it deploy/web -n participant-z -- curl backend.participant-x.svc.cluster.local -m 1 
```

</p>
</details>

