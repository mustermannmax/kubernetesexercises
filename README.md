## Kubernetes Workshop Exercises

## Table of Content

[Cheat Sheet](cheatsheet.md)

---

#### Exercises
* [1. Containers Basics](./container/container-basics.md)
* [2. Building Container Images](./container-images/container-images.md)
* [3. Setup and Basics](./setup-kubectl-basics/kubectl-basics.md)
* [4. Pods](./pods/pods.md)
* [5. Deployments, Rolling Updates](./deployments-rolling-updates/deployments-rolling-updates.md)
* [6. Services](services/services.md)
* [7. Ingresses with Nginx Ingress Controller](./ingress-nginx/ingress-nginx.md)
* [8. Environment Variables, Configmaps, Secrets](./env-vars-configmaps-secrets/configmaps-secrets-env.md)
* [9. Health, Resources, Lifecycle](./health-resources-lifecycle/health-resources-lifecycle.md)
* [10. Stateful Set, Persistent Storage](./stateful-set-persistent-storage/stateful-set-persistent-storage.md)
* [11. Helm](./helm/helm-chart.md#helm-12-using-helm-charts)
* [12. Building Helm Charts](./helm-building-a-helm-chart/helm-building-a-helm-chart.md)
* [13. ArgoCD](./argocd/gitops-example.md)
* [14. Gitlab CI](./gitlab-ci/gitlab-ci.md)
* [15. Prometheus, Grafana](./prometheus-grafana/prometheus-grafana.md)
* [16. NetworkPolicies](./network-policies/network-policies.md)


---

#### Challenges
* [Kubernetes Katas as Repetition for Day 2](challenges/kubernetes-katas.md)
* [Bug Hunt](challenges/bug-hunt/bug-hunt.md)
* [Deploy Wordpress with Helm](./challenges/beginner-wordpress-helm/wordpress-helm.md)
* [Mysql the Hard Way](./challenges/intermediate-mysql-the-hard-way/mysql-the-hard-way.md)
* [Redmine Helm Chart](./challenges/advanced-redmine-helm/redmine.md)

#### Notes for instructors:

Clean up after each exercise with
```
kubectl get ns | grep participant | cut -d' ' -f1 | while read -r namespace; do echo $namespace; kubectl get pod,svc,deploy,ing,pvc,statefulset,cronjob -o name -n $namespace | xargs kubectl delete -n $namespace ; done
kubectl get ns | grep participant | cut -d' ' -f1 | while read -r namespace; do helm ls -n $namespace | tail -n+2 | awk '{print $1}' | xargs -r helm delete -n $namespace ; done
```
