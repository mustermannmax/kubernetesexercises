# Services

Let's start by creating an arbitrary deployment, and our trusty debug pod:

```
kubectl create deployment hello --image=gcr.io/google-samples/hello-app@sha256:2b0febe1b9bd01739999853380b1a939e8102fd0dc5e2ff1fc6892c4557d52b9 --replicas 4
kubectl create deployment debug --image=praqma/network-multitool
```

---

**Note**: 
praqma/network-multitool is a container image containing lots of tools you might consider basic,
but are in fact often rightfully missing from docker images. (e.g., wget, curl, nslookup, tcpdump, traceroute,
 vi etc.)

---

## ClusterIP Service

Expose the deployment as a ClusterIP service

```
kubectl expose deployment hello --port 80 --target-port=8080 --type ClusterIP
```

Look at what was created
```
kubectl get svc
```

Now open a shell on the pod debug pod

```
kubectl exec --stdin --tty deployment/debug -- /bin/sh
```

---

**Note**: 
We used `svc` as abbreviation for `service`. You can list all available resource types and their abbreviations with 
`kubectl api-resources`

---

Try to `curl` the `ClusterIP` of the service, we created previously

```bash
curl -s <insert IP here>
```

Now try to call the service using DNS **(insert the name of your namespace, which you can get from your kubeconfig current contexts' namespace with `kubectl config get-contexts` )**.
bash
```
curl hello.NAMESPACE.svc.cluster.local
```

Log out of the shell using `exit` or with `CTRL-D`


## NodePort Service

Let's try out the NodePort Servicetype next. Create a Nodeport service using a manifest.

```bash
kubectl apply -f nodeport.yaml 
```

You can check out your service (and the port it opened on the nodes) with
```
kubectl get svc
```

---

 **Note**: 
Of course only one Service per Cluster can claim a specific port, e.g. 30007

---

We now have a cluster-wide port for this deployment on all worker nodes. Knowing the public IP of our nodes, we can directly communicate with it via this Pod from outside the cluster. Though those nodes didn't get external IPs because we solved the problem of communication with those nodes differently.


```bash
kubectl get nodes -o wide
```

Since those nodes don't have and EXTERNAL-IP, you have to use the INTERNAL-IP instead. Instead, open a shell on your `debug` container inside your cluster `kubectl run debug --rm -it --image=praqma/network-multitool -- /bin/bash`. Those internal IPs are not IPs inside the virtual kubernetes network but the azure vnet.

```bash
curl -s <NODE_INTERAL_IP>:<NODE_PORT>
```

You should try all the other node ips as well.

## LoadBalancer Service

---
 
 **Note**: 

There is a limit of public ip addresses allowed for each azure account, so there is a possibility that not everybody will be able to succeed in this exercise.

---
 
Now there is the final service type to communicate with a pod, the LoadBalancer Service.
Some cloud providers allow you to specify the loadBalancerIP. In those cases, the load-balancer is created with the user-specified loadBalancerIP. If the loadBalancerIP field is not specified, the loadBalancer is set up with an ephemeral IP address. If you specify a loadBalancerIP but your cloud provider does not support the feature, the loadbalancerIP field that you set is ignored.

```bash
kubectl expose deployment hello --port 80 --target-port 8080 --type LoadBalancer --name hello-loadbalancer
```

Note how the external IP is still showing pending.
```bash
kubectl get svc
```

In a few minutes you will be able to talk with the service without any port numbers.
```bash
kubectl get svc
```
```bash
curl <EXTERNAL IP>
```
## Headless Service

Let's try out the headless service last.

```bash
kubectl apply -f headless.yaml
```

Open a shell inside out debug pod again and ping the headless service
```bash
while true; do ping -q -c1 hello-headless-service; sleep 1; done
```
Note how the ip changes every time

Let's compare the dns record
```bash
nslookup hello
```
```bash
nslookup hello-headless-service
```

 ## ExternalName Service

There is a last service type, that is not used to connect with pods, but for providing different DNS names for external
services. It is often much cleaner to map external services this way, an example might be different external databases for
`dev`, `qa` and `prod`.

Apply the ExternalName Service you find in `external_name.yaml`
```bash
kubectl apply -f external_name.yaml
```

This service maps the website `neverssl.com` to an internal DNS name. We chose this website explicitly because it doesn't use `TLS` (and the name implies it will never do so either).
Open a terminal into your network-multitool container.

```bash
kubectl exec -it deploy/debug -- /bin/bash
```
```bash
curl never-ssl -s |  head  -n 20
```

The `neverssl.com` website is now available with the name `never-ssl` in your namespace and with `never-ssl.YOUR_NAMESPACE.svc.cluster.local`
on the whole cluster. Try a nslookup with the shortened url.

```bash
nslookup never-ssl
```

---
 
 **Note**: 

You should always try to use FQDNs like `never-ssl.YOUR_NAMESPACE.svc.cluster.local`

---

## Bonus Exercise 1: Canary Deployment

Cleanup the services again.
```bash
kubectl get svc -o name | xargs kubectl delete
```

The file `canary.yaml` contains two `Deployments` and one `Service` of type `ClusterIP. Start by deploying those manifests:

```bash
kubectl apply -f canary.yaml
```

---

**Note**:

You can use `k` if you don't want to type a little less, it is an alias for `kubectl` (`alias k=kubectl`). 

---

Look at the deployed resources using `kubectl get all`.

Why are there two `Deployments` but only one `Pod`?

Let's continuously call the `Service` `canary` to watch for changes in a separate terminal session. For this we start by spawning a pod inside the cluster.

```bash
k run debug --rm -it --image=praqma/network-multitool -- /bin/bash
```
```bash
while sleep 1; do curl canary:8080; done
```
---

Now scale up the deployment `canary-v2`:
```
kubectl scale deploy/canary-v2 --replicas 1
```
To finish our smooth version change, scale down the old deployment.

```
kubectl scale deploy/canary-v1 --replicas 0
```

Do you recognize some limitations with this approach to canary releases?

---
## Bonus Exercise 2: Find the error

First, cleanup the services again

```
kubectl get svc -o name | xargs kubectl delete
```

Deploy the service contained in `faulty.yaml`. There are several errors contained in this file. Can you find them?


