# ArgoCD

As shown in our brief Introduction to ArgoCD, it is a powerful CD Tool, capable of syncing GitOps Repositories on the fly.
Let's get our feet wet.


Login into the argocd CLI. You will receive the credentials from us.

```bash
argocd login argocd.mt-k8s.eu --grpc-web
```


### Applications
Let's have a look at `gitops-example/application.yaml`

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: echo-server-<USER_ID>
  namespace: argocd
  finalizers:
  - resources-finalizer.argocd.argoproj.io
spec:
  destination:
    namespace: participant-<USER_ID>
    server: https://kubernetes.default.svc
  project: default
  source:
    repoURL: https://gitlab.com/mt-ag-k8s-training-course/kubernetesexercises.git
    targetRevision: HEAD
    path: argocd/gitops-example/charts/echo-server
    helm:
     valueFiles:
        - values.yaml
  syncPolicy:
    automated:
      selfHeal: true
```


We declare an `Application` object, containing a helm-chart from a specifc `repoURl`. In our case this helm chart is placed in `echo-server` in the chart directory of this exercise. We also declare, which `values.yaml` to use.

Open the file `gitops-example/application.yaml` yourself and change `<USER_ID>` to your participant number.

Now let's try to create the app:

```bash
argocd app create -f gitops-example/application.yaml
```

Use `kubectl get deployments,pods` to see the result or use the `ArgoCD UI`.


### Syncing

We also defined a `syncPolicy` in our Application, which keeps the Repository synced at all times. We also want the Application to `selfHeal`, which means if anything we declared in this file is not deployed anymore eg. in deleting it manually, ArgoCD takes care to reapply our defined state.

Let's try that:

```bash
kubectl delete deploy echo-server-<USER_ID>
```

Observe again in `kubectl` or the `ArgoCD UI` what happens. 


### Exercise
To get an even better understanding in how ArgoCD reacts to Changes, we 
  - create our own GitOps repository based on this one 
  - edit `the application.yaml` to point to our new repository and adjust the path to the helm chart
  - IMPORTANT: make sure your new repository is not private, you can set it to public in:
    - Settings > General > Visibility... > Project Visibility 
  - delete, then deploy your echo-server application again:
    - `argocd app delete <application-name>`
    - `argocd app create -f application.yaml` 
  - make a change to the Helm Chart's values to deploy an `Ingress`
  - Commit and push the change to your new GitOps repository
  - observe the Results in either UI or CLI.

<details>
  <summary markdown="span"> TIP1: Steps to create your own GitOps-Repository </summary>
  
  - navigate out of `kubernetesexercises` 
  - `mkdir <your-new-repo-name> && cd <your-new-repo-name>`
  - `git init`
  - `git remote add origin https://gitlab.com/<USER_ID>/<your-new-repo-name>`.git
  - `cp -r ../kubernetesexercises/argocd/gitops-example/* .`
  - `git add .`
  - `git commit -a -m "Init new GitOps repository"`
  - `git push origin master`
    
</details>
<details>
 <summary markdown="span"> TIP2: Value File</summary>

 - there is a `my_values.yaml` within the echoserver chart that you can simply edit and add to your `valueFiles`.

</details>
