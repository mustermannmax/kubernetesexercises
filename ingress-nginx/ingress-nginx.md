# Ingress

Welcome to our exercise about the Kubernetes Ingress. The following exercises use files provided in `kubernetesexercises/ingress-nginx`. We will create a simple ingress, learn about the effects of different paths and the `pathType` attribute as well as try out using TLS with and without a verified certificate.

*IMPORTANT*
To prevent naming collisions with other participants, please run the following snippet after replacing `<YOUR_NAME_IN_LOWERCASE>` with something else. This will change a few occurences of the string `CHANGEME` with whatever you replaced `<YOUR_NAME_IN_LOWERCASE>` with in the YAML-files we provide for you.
```bash
find . -type f -exec sed -i "s/CHANGEME/<YOUR_NAME_IN_LOWERCASE>/g" {} \;
```

## The ingress-controller

In the lecture, we talked about the `ingress-controller`, a dedicated server that acts as a central entrypoint into the cluster. It is typically implemented as a Deployment with a number of replicas running a container image of some server software like `nginx`, `kong` or `traefik`. 

Since setting up an `ingress-controller` requires a certain knowledge of the server software it should run, we already did that for you. You can check it out by running

```bash
kubectl get all --namespace ingress-nginx
```

You will see both the `ìngress-controller` and a `LoadBalancer` service with a public IP that points to the aforementioned. From the naming you can already guess that we chose to run an `nginx` server for you. We also rented out a domain for this workshop and assigned it to the LoadBalancer's public IP: `*.mt-k8s.eu`

## A simple ingress

Applying an Ingress object in a cluster is basically just a comfortable way of configurating an `ingress-controller` without the complexity of understanding the underlying server software it runs.

Let's create an arbitrary deployment to work with and expose it to the cluster with a simple `ClusterIP` service:

```bash
kubectl create deployment hello-test --image=gcr.io/google-containers/echoserver:1.8
kubectl expose deployment hello-test --port=8080
```

A Deployment with a ClusterIP service is one of the most common and simple structures in a cluster. Now, let's try to make sure we can reach it from the public internet with a simple `Ingress` object:

```bash
kubectl apply -f ingress.yaml
```

You should now be able to call our demo application via the Ingress we just applied using [http://<YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/hello-world](http://<YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/hello-world).

```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/hello-world
```

## Exercise 1

Try out whether the following URLs do work or not. Can you explain why given the path rules set in the Ingress you deployed?

---
**Note**: Sometimes, it is useful to view the ingress controller logs for debugging:
```bash
kubectl logs -n ingress-nginx deploy/ingress-nginx-controller
```
---

```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/
```
<details><summary>Explanation</summary><p>Does not work. There is no rule that captures this path.</p></details>

---

```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/hello-world/this-workshop-rocks
```
<details><summary>Explanation</summary><p>Works. The URL path begins with "/<YOUR_NAME_IN_LOWERCASE>/hello-world/" and thus is covered by the Prefix-path.</p></details>

---

```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/hello-world-and-the-universe
```
<details><summary>Explanation</summary><p>Does not work. The URL path may begin with "/<YOUR_NAME_IN_LOWERCASE>/hello-world" but is not covered by the Prefix-path because there is no / between "hello-world" and the remaining "-and-the-universe".</p></details>

---

```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/other-people/usually-like-this-workshop
```
<details><summary>Explanation</summary><p>Reading Regex can be tricky but we want to show you that it is allowed to do so. This URL path does not work because the Regex requires one or more / after "other" and whatever follows. </p></details>


```bash
curl <YOUR_NAME_IN_LOWERCASE>.mt-k8s.eu/<YOUR_NAME_IN_LOWERCASE>/other/people-usually-like-this-workshop
```
<details><summary>Explanation</summary><p>This one works, because it follows the Regex pattern provided.</p></details>

---

## Encrypting Traffic using TLS

So far, our Ingress works only with unencrypted HTTP traffic. Changing this in a learning environment like our workshop does not require much effort. Take a look at `ingress-tls.yaml` and observe the fields under `metadata.annotations` and `spec.tls`. Do you know what or who `letsencrypt` is?

<details><summary>Explanation</summary><p>
"Let's encrypt" is a non-profit certificate authority that provides TLS certificates for public use. Their services allow us to provide you with the opportunity to do this exercise (:
</p></details>

Applying this `ingress-tls.yaml` will not only create an Ingress object but request e certificate and key from `Let's Encrypt` to secure your traffic. We can observe this by running a watcher on the `certificate` and `certificaterequest` objects in our Kubernetes namespace:

```bash
kubectl get certificate -w & kubectl get certificaterequest -w
```

Now, apply the file using a second shell:
```bash
kubectl apply -f ingress-tls.yaml
```
Wait and observe how the `certificaterequest` is automatically handled by Kubernetes and wait until the `certificate` is ready. Now, query your URL using the browser of your choice. Can you confirm that the ingress' certificate has been issued by `Let's encrypt`?

## Self-signed Certificates

Of course, you can also issue your ingress to use a self-signed certificate. Note the differences in `ingress-tls-no-certificate.yaml` and apply it:

```bash
kubectl apply -f ingress-tls-no-certificate.yaml
```

Try to call the ingress from your browser or with curl:
```bash
curl https://<YOUR_NAME_IN_LOWERCASE>-no-valid-cert.mt-k8s.eu
```
You will be notified about an invalid certificate since the self-signed default certificate is used. Still, you can communicate with the underlying application when allowing insecure certificates:

```bash
curl --insecure https://<YOUR_NAME_IN_LOWERCASE>-no-valid-cert.mt-k8s.eu
```

## Exercise 2

Update the `ingress-tls-no-certificate.yaml` file so it uses a certificate issued by "Let's Encrypt". Can you remember the required fields? If not, use `ingress-tls.yaml` as reference.

Pro tip:
If it does not work immediately, try emptying your browser's cache or opening it in another browser that has not yet cached the non-TLS version ;)

<details><summary>Solution</summary>
<p>

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    ...
    cert-manager.io/cluster-issuer: letsencrypt
spec:
...
  tls:
    - hosts:
        -  CHANGEME-no-valid-cert.mt-k8s.eu
      secretName: CHANGEME-no-valid-cert-mt-k8s-eu
```

</p>
</details>

## Bonus Exercise: Canaries with Errors

Canary releases are a popular way of trying out new versions of a software. In a nutshell, you run both the old and the new version in parallel and split the incoming traffic between the two based on certain factors, e.g. 20% of traffic to the new version, the rest to the old.

Let's try out to set this up using Kubernetes Ingress objects. **We will use the `hello-test`-`Deployment` and -`Service` we created at the start of the exercise.** If you deleted them already, please apply them again.

Now, let's create another deployment and service to represent our "new version":
```
kubectl create deployment hello-testv2 --image=gcr.io/google-samples/hello-app@sha256:2b0febe1b9bd01739999853380b1a939e8102fd0dc5e2ff1fc6892c4557d52b9
kubectl expose deployment hello-testv2 --port=8080
```

The file `ingress-canary.yaml` is a failed attempt at creating a canary ingress. It defines one Ingress for each of the two Deployments. We made three mistakes hidden in the **first ingress** of the two contained in this file. **Can you fix them?**

<details><summary>spoiler - solution</summary>
<p>

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: hello-canary-1
  annotations:
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  ingressClassName: nginx # Ingress class name is missing
  rules:
    - host: CHANGEME.mt-k8s.eu
      http:
        paths:
          - path: /canary
            pathType: Prefix
            backend:
              service:
                name: hello-test # Wrong Service Name
                port:
                  number: 8080 # Wrong Port Name
```

</p>
</details>

Try to reload the ingress domain and path several times. Do you see the canary working?
