Cheatsheet - Commonly used kubectl Commands
```
kubectl config get-contexts                              # See all available contexts
kubectl ctx                                              # See all available contexts using krew ctx plugin
kubectl config view                                      # See current cluster context
kubectl config set-context $(kubectl config current-context) --namespace=some-namespace
                                                         # Change default namespace
kubectl ns                                               # See all available namespaces
kubectl ns some-namespace                                # Change default namespace using krew ns

kubectl help run                                         # See help about run (or other commands)
kubectl explain pod.spec                                 # Documenation on any resource attribute

kubectl get nodes  -o wide                               # See nodes in cluster with ips
kubectl get pods -o wide                                 # See pods in current namespace
kubectl get svc <name> -o yaml                           # See info about service <name> in yaml format
kubectl describe pod <name>                              # Show information about pod <name>
kubectl describe service <name>                          # Show information about service <name>

kubectl api-resources                                    # See resources types and abbreviations

kubectl create namespace some-namespace                    # Create namespace
                                                         # Set default namespace
kubectl config set-context $(kubectl config current-context) --namespace=<some-namespace>

kubectl run multitool --image=praqma/network-multitool --restart Never   # Create plain pod
kubectl create deployment nginx --image=nginx:1.7.9      # Create deployment

kubectl set image deployment/nginx nginx=nginx:1.9.1     # Update image in deployment pod template
kubectl scale deployment nginx --replicas=4              # Scale deployment
kubectl rollout status deployment/nginx                  # See rollout status
kubectl rollout undo deployment/nginx                    # Undo a rollout

kubectl run <name> --dry-run=client --image <image> -o yaml > <pod>.yaml # generate yaml-file for pod

kubectl exec -it <name> bash                             # Execute bash inside pod <name>
kubectl exec -it <name> -- my-cmd -with -args            # Execute cmd with arguments inside pod

kubectl delete pod <name>                                # Delete pod with name <name>
kubectl delete deployment <name>                         # Delete deployment with name <name>

kubectl expose deployment envtest --type=NodePort --port=3000  # Create service, expose deployment

kubectl create configmap language --from-literal=FOO=BAR # Create configmap
                                                         # Create secret
kubectl create secret generic apikey --from-literal=BAR=foo
```
Cheatsheet - Commonly used helm Commands
```

helm ls                                                  # List deployed helm charts

helm fetch CHART_NAME --untar                            # Fetch a helm chart and unpack it

helm lint                                                # Lint a chart, also checking for some errors

helm template .                                          # Render all templates of chart in local folder

helm upgrade --install --dry-run --debug . $RELEASE_NAME # Deploy a chart as a dry run

helm upgrade --install $RELEASE_NAME --namespace $NAMESPACE -f $CUSTOM_VALUES_YML $CHART_NAME # Deploy a chart

helm delete $RELEASE_NAME                                # Delete a chart


```
